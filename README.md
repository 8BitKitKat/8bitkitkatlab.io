# 8bitkitkat.gitlab.io

## Hay!

Thank you for having a look at my website source,

I plan to use this website as a portfolio, a place to practice and a general all purpose place for things that don't warrant their own site.

I'll add more to the site with time.

Thanks for having a look, go see the site if you haven't yet!

## Remotes

The website itself is hosted on gitlab but the source is mirrored to both gitlab and github:

https://gitlab.com/8BitKitKat/8bitkitkat.gitlab.io

https://github.com/8bitkitkat/8bitkitkat.gitlab.io
